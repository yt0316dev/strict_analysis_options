# strict_analysis_options

## Usage

### pubspec.yaml

```yaml
dev_dependencies:
  strict_analysis_options:
    git:
      ref: main
      url: https://gitlab.com/yt0316dev/strict_analysis_options.git
```

### analysis_options.yaml

```yaml
include: package:strict_analysis_options/analysis_options.yaml
```
