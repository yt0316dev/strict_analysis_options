#!/bin/sh

set -e

cd "$(dirname "$0")"

curl \
  -L \
  -o lib/analysis_options_all.yaml \
  https://raw.githubusercontent.com/dart-lang/sdk/refs/heads/main/pkg/linter/example/all.yaml
